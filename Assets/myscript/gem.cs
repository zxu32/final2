﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class gem : MonoBehaviour {
    
    public string gemType;
    // determines the amount of attack or defense the heros is going to get
    public int amount = 3;
    public hero hero;
    
    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if(gemType.Equals("red"))
        {
            hero.SetAttack(amount);
        }
        else
        {
            hero.SetDefense(amount);
        }
        this.gameObject.SetActive(false);
    }
}
