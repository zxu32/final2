﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class door : MonoBehaviour {
    public string doorType = "red";
    public hero hero;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {

        Vector3 lastPosition = hero.GetLastPosition();
        switch (doorType)
        {
            case "red":
                if (hero.GetKeyRed() > 0)
                {
                    hero.SetKey("red", -1);
                    this.gameObject.SetActive(false);
                    return;
                }
                break;
                
            case "blue":
                if (hero.GetKeyBlue() > 0)
                {
                    hero.SetKey("blue", -1);
                    this.gameObject.SetActive(false);
                    return;
                }
                break;

            case "yellow":
                if (hero.GetKeyYellow() > 0)
                {
                    hero.SetKey("yellow", -1);
                    this.gameObject.SetActive(false);
                    return;
                }
                break;
            default:
                
                hero.transform.position = lastPosition;
                break;
        }

        hero.transform.position = lastPosition;
    }
}
