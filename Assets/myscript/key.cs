﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class key : MonoBehaviour {
    public string keyType = "red";
    public hero hero;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        hero.SetKey(keyType, 1);
        this.gameObject.SetActive(false);
    }

}
