﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hero : MonoBehaviour {

    // the initial basic information for a hero
    public int hp = 0;
    public int attack = 0;
    public int defense = 0;
    public int level = 0;
    public int exp = 0;
    public int gold = 0;
    public int keyBlue = 0;
    public int keyYellow = 0;
    public int keyRed = 0;
    float lastMoveTime;
    Vector3 lastPosition = new Vector3(6, 0, 1);
    // return the hp status
    public int GetHp()
    {
        return hp;
    }
    // return the attack status
    public int GetAttack()
    {
        return attack;
    }
    // return the defense status
    public int GetDefense()
    {
        return defense;
    }
    // return the level status
    public int GetLvl()
    {
        return level;
    }
    // return the exp status
    public int GetExp()
    {
        return exp;
    }
    // return the gold status
    public int GetGold()
    {
        return hp;
    }
    // return the keyBlue status
    public int GetKeyBlue()
    {
        return keyBlue;
    }
    // return the keyYellow status
    public int GetKeyYellow()
    {
        return keyYellow;
    }
    // return the keyRed status
    public int GetKeyRed()
    {
        return keyRed;
    }
    // add gold to hero
    public void SetGold(int amount)
    {
        gold = gold + amount;
    }
    // add gold to hero
    public void SetExp(int amount)
    {
        exp = exp + amount;
    }
    // enhance attack damage when the hero gets a red gem
    public void SetAttack(int amount)
    {
        attack = attack + amount;
    }
    // enhance defense when the hero gets a blue gem
    public void SetDefense(int amount)
    {
        defense = defense + amount;
    }
    // change hp, if the hero drinks potion, he gains hp, if he battles a monster he loses hp
    public void SetHp(int amount)
    {
        hp = hp + amount;
    }
    // level up
    public void SetLevel()
    {
        if(exp >= 100)
        {
            exp = exp - 100;
            level = level + 1;
            attack = attack + 10;
            defense = defense + 10;
        }
    }
    // get a key and identify the type
    public void SetKey(string type, int amount)
    {
        int comparison = string.Compare(type, "yellow", ignoreCase: true);
        if (comparison == 0)
        {
            keyYellow = keyYellow + amount;
            return;
        }
        comparison = string.Compare(type, "red", ignoreCase: true);
        if (comparison == 0)
        {
            keyRed = keyRed + amount;
            return;
        }
        comparison = string.Compare(type, "blue", ignoreCase: true);
        if (comparison == 0)
        {
            keyBlue = keyBlue + amount;
            return;
        }
    }

 

    public Vector3 GetLastPosition()
    {
        return lastPosition;
    }

    // Use this for initialization
    void Start() {
        hp = 1000;
        attack = 10;
        defense = 10;
        level = 1;
        exp = 0;
        gold = 0;
        keyBlue = 0;
        keyYellow = 0;
        keyRed = 0;
        lastMoveTime = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        detectMove();
}
    



    void detectMove()
    {
        if (Time.time - lastMoveTime > 0.15)
        {
            float inputH = Input.GetAxis("Horizontal");
            float inputV = Input.GetAxis("Vertical");
            Vector3 move = new Vector3(0, 0, 0);
            if (inputH != 0)
                move = new Vector3((inputH > 0 ? 1 : -1) * 1f, 0, 0);
            else if (inputV != 0)
                move = new Vector3(0, 0, (inputV > 0 ? 1 : -1) * 1f);
            lastPosition = transform.position;
            transform.position += move;
            lastMoveTime = Time.time;
        }
    }
}
