﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class potion : MonoBehaviour {
    public string potionType;
    public hero hero;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (potionType.Equals("red"))
            hero.SetHp(200);
        else
            hero.SetHp(500);
        this.gameObject.SetActive(false);
    }
}
